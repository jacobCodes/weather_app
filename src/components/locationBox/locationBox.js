const LocationBox = (props) => {
    const dateBuilder = (d) => {
      let months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];
      let days = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
      ];

      let day = days[d.getDay()];
      let date = d.getDate();
      let month = months[d.getMonth()];
      let year = d.getFullYear();
      return `${day} ${date} ${month} ${year}`;
    };
    return (
      <div>
        {typeof props.weatherReport.main != "undefined" ? (
          <>
            <div className="location-box">
              <div className="location">{props.weatherReport.name}</div>

              <div className="date">{dateBuilder(new Date())}</div>
            </div>
            <div className="weather-box">
              <div className="temp">
                {Math.round(props.weatherReport.main.temp)}°c
              </div>
              <div className="weather">{props.weatherReport.weather[0].main}</div>
            </div>
          </>
        ) : (
          <div className="intro">
            <div className="introHead">Weather Report with city</div>
          </div>
        )}
      </div>
    );
}

export default LocationBox;