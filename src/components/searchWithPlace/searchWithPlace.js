import { useState } from "react";
import LocationBox from "../locationBox/locationBox";

const SearchWithPlace = (props) => {
  const [query, setQuery] = useState("");
  const [weatherReport, setWeatherReport] = useState({});
  const search = (evt) => {
    if (evt.key === "Enter") {
      fetch(`${props.api.base}weather?q=${query}&units=metric&APPID=${props.api.key}`)
        .then((res) => res.json())
        .then((result) => {
          setWeatherReport(result);
          setQuery("");
        });

    }
  };
    return (
      <div>
        <div
          className={
            typeof weatherReport.main != "undefined"
              ? weatherReport.main.temp > 16
                ? "app warm"
                : "app"
              : "app"
          }
        >
          <main>
            <div className="search-box">
              <input
                type="text"
                className="search-bar"
                placeholder="Search..."
                onChange={(e) => setQuery(e.target.value)}
                value={query}
                onKeyPress={search}
              />
            </div>
            <LocationBox weatherReport={weatherReport} />
          </main> 
        </div>
      </div>
    );
}

export default SearchWithPlace;