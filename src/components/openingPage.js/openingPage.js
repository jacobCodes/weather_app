import { useState } from "react";

const OpeningPage = () => {
    const [latitude, setLatitude] = useState("")
    const[longitude, setLongitude] = useState("")
    const success = (pos) => {
        const cords = pos.coords;
        setLatitude(cords.latitude)
        setLongitude(cords.longitude);
        }
    const error = (error) => {
    }

    console.log(latitude)
    console.log(longitude)
    navigator.geolocation.getCurrentPosition(success, error);
    return (
        <div>
            <p>latitude:{latitude}</p>
            <p>longitude:{ longitude}</p>
        </div>
    )
}

export default OpeningPage;