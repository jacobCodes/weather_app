const LandingPage = (props) => {
    const data = props.charactersData.map((info, index) => {
        return (
          <div>
            <p>name:{info.name}</p>
            <p>role:{info.job}</p>
                <p>id:{info.id}</p>
                <button onClick={() => props.addToCounter()}>Add to cart</button>
          </div>
        );
    })
    return data
}

export default LandingPage