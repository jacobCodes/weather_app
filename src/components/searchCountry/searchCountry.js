import "../searchCountry/searchCountry.css"

function SearchCountry() {
    return (
        <main>
            <div className="search-box">
                <input type="text" className="search-bar" placeholder="search... "/>
            </div>
        </main>
    )
}

export default SearchCountry;