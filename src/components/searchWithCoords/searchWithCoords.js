import userEvent from "@testing-library/user-event";
import { useEffect, useState } from "react";
import LocationBox from "../locationBox/locationBox";

const SearchWithCoords = (props) => {
  const [weatherReportWithCoords, setWeatherReportWithCoords] = useState({});

  console.log(props, "props in searchwithcoords")
  
  const search =async () => {
   await   fetch(
        `${props.api.base}weather?lat=${props.lat}&lon=${props.log}&appid=${props.api.key}`
      )
        .then((res) => res.json())
        .then((result) => {
          setWeatherReportWithCoords(result);
        });
    console.log(weatherReportWithCoords,"weather report with coords")
  };
    
  useEffect(() => {
    if (!props.loactionDisable && props.lat) {
        search();
      } 
    },[props]);

  return (
    <div>
      <div
        className={
          typeof weatherReportWithCoords.main != "undefined"
            ? weatherReportWithCoords.main.temp > 16
              ? "app warm"
              : "app"
            : "app"
        }
      >
        <main>
          <LocationBox weatherReport={weatherReportWithCoords} />
        </main>
      </div>
    </div>
  );
}

export default SearchWithCoords;