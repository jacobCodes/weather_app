import SearchWithPlace from "./components/searchWithPlace/searchWithPlace";
import { useState } from "react";
import SearchWithCoords from "./components/searchWithCoords/searchWithCoords";

//  https://api.openweathermap.org/data/2.5/weather?q={city name}&appid={API key}
const api = {
  key: "0c72021ab81f18c55b38820da1672f43",
  base: "https://api.openweathermap.org/data/2.5/",
};

function App() {
   const [latitude, setLatitude] = useState("");
  const [longitude, setLongitude] = useState("");
  const [loactionDisable,setLocationDisable] = useState(false)

   const success = (pos) => {
     const cords = pos.coords;
     setLatitude(cords.latitude);
     setLongitude(cords.longitude);
   };
  const error = (error) => {
     setLocationDisable(true)
   };
  navigator.geolocation.getCurrentPosition(success, error);
  return (
    <div>
      {loactionDisable === true ? (
        <SearchWithPlace api={api} />
        
      ) : (
          
          <SearchWithCoords
          lat={latitude}
          log={longitude}
          api={api}
          loactionDisable={loactionDisable} />
      )}
    </div>
  );
}

export default App;
